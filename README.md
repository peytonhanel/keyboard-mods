Python script for Linux that enables QWERTY shortcuts to be used with a Dvorak
keyboard. Pressing super + spacebar will switch between Qwerty and dvorak.
This was developed on Ubuntu (pop-os) so it might not work for every distro,
but that can be easily fixed by changing the command in the script that
does the switching.

I personally don't run this manually, I have a command in my startup applications
that runs it in the background using nohup.

```
/bin/bash -c "nohup python ~/scripts/dvorak-with-qwerty-shortcuts.py > /dev/null &"
```


# Developer Info

Created by Peyton Hanel
