import os
from pynput import keyboard

# dvorak is initial keyboard on startup
os.system('setxkbmap dvorak')

set_to_qwerty = False
cmd_is_down = False

def on_press(key):

    global set_to_qwerty
    global cmd_is_down

    # switch to US temporarily for shortcuts
    if not set_to_qwerty and (key == keyboard.Key.ctrl or key == keyboard.Key.ctrl_r or key == keyboard.Key.cmd):
        os.system('setxkbmap us')

    # tell us if we are holding cmd
    if key == keyboard.Key.cmd:
        cmd_is_down = True

    # if holding cmd and we hit space, change layout for typing
    if cmd_is_down and key == keyboard.Key.space:
        if not set_to_qwerty:
            os.system('setxkbmap us')
            set_to_qwerty = True
        else:
            os.system('setxkbmap dvorak')
            set_to_qwerty = False


def on_release(key):

    global set_to_qwerty
    global cmd_is_down
    global caps_switch

    if not set_to_qwerty and (key == keyboard.Key.ctrl or key == keyboard.Key.ctrl_r or key == keyboard.Key.cmd):
        os.system('setxkbmap dvorak')

    if key == keyboard.Key.cmd:
        cmd_is_down = False


with keyboard.Listener(
        on_press=on_press,
        on_release=on_release) as listener:
    listener.join()
